// HMR не умеет во множественные точки входа,
// поэтому в entry нужно добавлять пути до дев-сервера

import pages from './pages';
import address from './address';

function setEntries(env = 'dev') {
  const entriesObj = {};

  pages.forEach((pageName) => {
    const isIndex = pageName === 'index';
    const allPath = `./source/pages/${pageName}/index.js`;
    const indexPath = './source/pages/index/index.js';
    const pagePath = (isIndex) ? indexPath : allPath;
    const host = `${address.ip}:${address.port}`;

    if (env !== 'dev') {
      entriesObj[pageName] = [
        pagePath,
        'webpack/hot/only-dev-server',
        `webpack-dev-server/client?http://${host}`,
      ];
    } else {
      entriesObj[pageName] = pagePath;
    }
  });

  return entriesObj;
}

const entriesDev = setEntries();
const entriesBuild = setEntries('build');

export { entriesDev, entriesBuild };
