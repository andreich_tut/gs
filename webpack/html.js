import HTML from 'html-webpack-plugin';
import pages from './pages';

const pagesArr = [];

pages.forEach((pageName) => {
  const isIndex = pageName === 'index';
  const chunksArr = (isIndex) ? ['index'] : ['index', pageName];
  const curPage = new HTML({
    filename: `${pageName}.html`,
    chunks: chunksArr,
    template: `./source/pages/${pageName}/index.pug`,
  });
  pagesArr.push(curPage);
});

export default () => ({ plugins: pagesArr });