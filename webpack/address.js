import os from 'os';

const net = os.networkInterfaces();
let currentIp = null;

Object.keys(net).forEach((ifname) => {
  net[ifname].forEach((iface) => {
    if (iface.family !== 'IPv4' || iface.internal !== false) return;
    currentIp = iface.address;
  });
});

export default {
  ip: currentIp,
  port: 3000,
};