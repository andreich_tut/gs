import 'fullpage.js/dist/fullpage.css';
import 'fullpage.js/vendors/scrolloverflow';
import FullPage from 'fullpage.js';
import Circles from 'modules/Circles';
import Gstudio from 'modules/Gstudio';
import 'modules/Rewards';
import { fullPageScrollHandler, initRolls, setScrollPaddingMobile } from 'modules/Projects';
import ShowreelVideo from 'modules/Showreel';
import Journal from 'modules/Journal';
import 'modules/Contacts';
import './index.scss';

function animateItems(source, show = false) {
  const hidden = source.item.querySelectorAll('.js-hidden');
  [...hidden].forEach((elem, i) => {
    setTimeout(() => {
      if (show) {
        elem.classList.add('js-show');
      } else {
        elem.classList.remove('js-show');
      }
    }, 40 * i);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  Gstudio();

  setScrollPaddingMobile();
  initRolls();

  const isMobile = window.innerWidth < 1280;

  // ставим постраничный скролл
  const mainFullPage = new FullPage('#fullPage', {
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
    sectionSelector: '.section',
    navigation: false,
    anchors: ['gstudio', 'rewards', 'showreel', 'projects', 'journal', 'contacts'],
    normalScrollElements: '.js-normal-scroll',
    scrollingSpeed: 900,

    afterLoad: (origin, destination) => {
      const contactsLink = document.querySelector('.js-contacts-link');
      const isContacts = destination.anchor === 'contacts';
      const isProjects = (destination) ? destination.anchor === 'projects' : false;

      if (isProjects) {
        mainFullPage.setAllowScrolling(false);
      }

      if (isContacts && isMobile) {
        contactsLink.classList.add('js-hidden');
      } else {
        contactsLink.classList.remove('js-hidden');
      }

      animateItems(destination, true);
    },

    onLeave: (origin) => {
      if (origin.anchor === 'journal') {
        const list = document.querySelector('#projectsList');
        list.scrollTop = 1;
      }
      Circles();
      animateItems(origin);
    },
  });

  ShowreelVideo(mainFullPage);
  Journal(mainFullPage);
  fullPageScrollHandler(mainFullPage);
});
