import Instafeed from 'instafeed.js';
import Tingle from 'tingle.js';
import './index.scss';

function truncateWithEllipses(text, max) {
  return text.substr(0, max - 1) + (text.length > max ? '&hellip;' : '');
}

function initModal(item, fullPage) {
  const image = item.querySelector('img');
  const caption = image.getAttribute('data-caption');
  const link = image.getAttribute('data-link');
  const source = image.getAttribute('src');
  const symbolsCount = (window.innerWidth < 1280) ? 150 : 300;
  const desc = truncateWithEllipses(caption, symbolsCount);
  const modal = new Tingle.modal({
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: 'Close',
    cssClass: ['journal'],
    onOpen: () => {
      fullPage.setAllowScrolling(false); // вырубаем скролл при открытии модалки
      const instance = document.querySelector('.tingle-modal--visible');
      const hidden = instance.querySelectorAll('.js-hidden');
      [...hidden].forEach(el => el.classList.add('js-show'));
      const closeBtn = instance.querySelector('.modal-close');
      closeBtn.addEventListener('click', () => {
        [...hidden].forEach(el => el.classList.remove('js-show'));
        modal.close();
      });
    },
    onClose: () => {
      fullPage.setAllowScrolling(true); // врубаем скролл при открытии модалки
    },
  });
  const content = `
    <div class="instagrid-modal">
      <svg class="modal-close js-hidden" width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="1.04956" y="41.3604" width="1.4" height="57.008" transform="rotate(-135 1.04956 41.3604)" fill="white"/>
        <rect x="41.3604" y="40.3701" width="1.4" height="57.008" transform="rotate(135 41.3604 40.3701)" fill="white"/>
      </svg>
      <img class="image js-hidden" src=${source}>
      <div class="desc js-hidden"><p>${desc}</p><a class="link" href="${link}" target="_blank">See in Instagram</a></div>
    </div>
  `;

  modal.setContent(content);
  item.addEventListener('click', () => modal.open());
}


export default function (fullPage) {
  const limit = (window.innerWidth < 1280) ? 12 : 15;

  const feed = new Instafeed({
    get: 'user',
    userId: '1912819634',
    clientId: '1912819634',
    accessToken: '1912819634.0ab4d5d.211f758f51664524b4e798d0e424055b',
    target: 'instagrid',
    limit,
    resolution: 'standard_resolution',
    template:
        '<div class="instagrid__item js-hidden">' +
        '<img class="image" src="{{image}}" data-caption="{{caption}}" data-link="{{link}}">' +
        '</div>',
    after: () => {
      const parent = document.querySelector('#instagrid');
      const { children } = parent;
      [...children].forEach(item => initModal(item, fullPage));
    },
  });
  feed.run();
}