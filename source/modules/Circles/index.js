import './index.scss';

const circles = document.querySelector('#circles');
const scaled = 'scaled';
const { innerWidth } = window;

export default function () {
  if (innerWidth > 1280) {
    circles.classList.add(scaled);
    setTimeout(() => {
      circles.classList.remove(scaled);
    }, 400);
  }
}