import './index.scss';

function innerClick(e, roll) {
  const isActive = roll.classList.contains('active');
  const list = roll.querySelector('.js-roll-list');
  let height = 0;

  if (isActive) {
    roll.classList.remove('active');
  } else {
    roll.classList.add('active');
  }

  const isTrullyActive = roll.classList.contains('active');
  if (isTrullyActive) height = list.scrollHeight;
  list.style.height = `${height}px`;
}

function outerClick(e, roll) {
  const { target } = e;
  const isTarget = target.matches('.js-roll, .js-roll > *');

  if (!isTarget) {
    const list = roll.querySelector('.js-roll-list');
    roll.classList.remove('active');
    list.style.height = '0px';
    document.body.removeEventListener('click', outerClick);
  }
}

function rollHandler(roll) {
  roll.addEventListener('click', (rollEvent) => {
    innerClick(rollEvent, roll);
    document.body.addEventListener('click', e => outerClick(e, roll));
  });
}

function initRolls() {
  const rollers = document.querySelectorAll('.js-roll');
  if (!rollers) return;
  [...rollers].forEach(roll => rollHandler(roll));
}

function fullPageScrollHandler(fullPage) {
  const projectList = document.querySelector('#projectsList');
  projectList.addEventListener('scroll', (e) => {
    const { scrollTop, scrollHeight, clientHeight } = e.currentTarget;
    const bottomPos = scrollTop + clientHeight;

    if (scrollTop === 0) {
      fullPage.moveTo('showreel');
      setTimeout(() => {
        fullPage.setAllowScrolling(true);
      }, 150);
    }
    if (bottomPos >= scrollHeight - 30) {
      fullPage.moveTo('journal');
      setTimeout(() => {
        fullPage.setAllowScrolling(true);
      }, 150);
    }
  });
}

function setScrollPaddingMobile() {
  const { innerWidth } = window;
  if (innerWidth < 767) {
    const projectList = document.querySelector('#projectsList');
    const parentHeight = projectList.parentElement.scrollHeight;
    const padding = (parentHeight / 2) - 17;
    projectList.setAttribute('style', `padding-top: ${padding}px`);
  }
}

export { initRolls, fullPageScrollHandler, setScrollPaddingMobile };