import Vimeo from '@vimeo/player';
import Tingle from 'tingle.js';
import 'tingle.js/src/tingle.css';
import './index.scss';

export default function (fullPage) {
  const play = document.querySelector('#showreelPlay');
  const content = `
    <div class="video-cont" id="showreelVideoCont">
      <svg class="modal-close js-showreel-close js-hidden" width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="1.04956" y="41.3604" width="1.4" height="57.008" transform="rotate(-135 1.04956 41.3604)" fill="white"/>
        <rect x="41.3604" y="40.3701" width="1.4" height="57.008" transform="rotate(135 41.3604 40.3701)" fill="white"/>
      </svg>
    </div>
  `;

  const header = document.querySelector('.js-header');
  const contactsLink = document.querySelector('.js-contacts-link');

  const isMobile = window.innerWidth < 1280;

  const videoOptions = {
    id: 230430161,
    width: 720,
    autoplay: true,
  };
  let player;

  const modal = new Tingle.modal({
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: 'Close',
    cssClass: ['showreel'],
    onOpen: () => {
      const closeBtn = document.querySelector('.js-showreel-close');
      const title = document.querySelector('.js-showreel-title');
      closeBtn.classList.remove('js-hidden');
      closeBtn.addEventListener('click', () => modal.close());
      contactsLink.classList.add('js-hidden');
      if (!isMobile) title.classList.remove('js-show');
      if (isMobile) header.classList.add('js-show');
      fullPage.setAllowScrolling(false); // отрубаем постраничный скролл пока открыт видос
      player = new Vimeo('showreelVideoCont', videoOptions);
      player.play();
    },
    onClose: () => {
      const closeBtn = document.querySelector('.js-showreel-close');
      const title = document.querySelector('.js-showreel-title');
      contactsLink.classList.add('js-hidden');
      closeBtn.classList.add('js-hidden');
      if (!isMobile) title.classList.add('js-show');
      if (isMobile) header.classList.remove('js-show');
      fullPage.setAllowScrolling(true); // врубаем постраничный скролл когда закрыт видос
      player.pause();
    },
  });


  modal.setContent(content);
  play.addEventListener('click', () => modal.open());
}