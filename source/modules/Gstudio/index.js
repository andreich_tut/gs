import './index.scss';

const title = 'Gstudio';
const canvas = document.querySelector('#mainTitle');
const ctx = canvas.getContext('2d');
const maxLength = 7;
const time = 25;
const { innerWidth } = window;
const isLaptop = innerWidth < 1800;
const isMobile = innerWidth < 768;

ctx.canvas.width = (isMobile) ? 200 : 500;
ctx.canvas.height = (isMobile) ? 60 : 150;

const { width, height } = ctx.canvas;
let counter = 0;
let clickCounter = 0;

function setFont(trash = false) {
  const fontName = (trash) ? 'icomoon' : 'GrotesqueBold';
  let fontSize = (trash) ? 72 : 92;
  if (isLaptop) fontSize = (trash) ? 48 : 62;
  if (isMobile) fontSize = (trash) ? 24 : 32;
  ctx.fillStyle = '#ffffff';
  ctx.font = `${fontSize}px ${fontName}`;
}

function getRandChar() {
  const rand = Math.floor(97 + (Math.random() * 24));
  return String.fromCharCode(rand);
}

function setChars(trash = true) {
  const rnd = Math.floor((Math.random() * (maxLength - 1)) + 1);
  let text = '';

  for (let i = 0; i < rnd; i += 1) {
    text += getRandChar();
  }

  if (counter > time) text = title;

  ctx.clearRect(0, 0, width, height);
  setFont(trash);
  ctx.fillText(text, 0, height / 2);

  return text;
}

function animate() {
  if (counter > time) {
    setChars(false);
    counter = 0;
    return;
  }
  counter += 1;
  setChars();

  setTimeout(() => {
    window.requestAnimationFrame(animate);
  }, 1000 / 60);
}

export default function () {
  setFont();
  ctx.textAlign = 'left';
  ctx.textBaseline = 'center';
  ctx.fillText(title, 0, height / 2);

  canvas.addEventListener('click', () => {
    if (clickCounter >= 2) {
      animate();
      clickCounter = 0;
    } else {
      clickCounter += 1;
    }
  });
}